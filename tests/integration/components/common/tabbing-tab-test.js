import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('common/tabbing-tab', 'Integration | Component | common/tabbing tab', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{common/tabbing-tab}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#common/tabbing-tab}}
      template block text
    {{/common/tabbing-tab}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
