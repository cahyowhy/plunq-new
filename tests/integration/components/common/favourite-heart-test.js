import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('common/favourite-heart', 'Integration | Component | common/favourite heart', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{common/favourite-heart}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#common/favourite-heart}}
      template block text
    {{/common/favourite-heart}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
