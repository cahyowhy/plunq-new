import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('common/variabel-width-slick', 'Integration | Component | common/variabel width slick', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{common/variabel-width-slick}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#common/variabel-width-slick}}
      template block text
    {{/common/variabel-width-slick}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
