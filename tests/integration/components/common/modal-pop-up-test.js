import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('common/modal-pop-up', 'Integration | Component | common/modal pop up', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{common/modal-pop-up}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#common/modal-pop-up}}
      template block text
    {{/common/modal-pop-up}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
