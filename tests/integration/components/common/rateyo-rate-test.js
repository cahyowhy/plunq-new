import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('common/rateyo-rate', 'Integration | Component | common/rateyo rate', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{common/rateyo-rate}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#common/rateyo-rate}}
      template block text
    {{/common/rateyo-rate}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
