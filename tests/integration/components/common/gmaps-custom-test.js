import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('common/gmaps-custom', 'Integration | Component | common/gmaps custom', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{common/gmaps-custom}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#common/gmaps-custom}}
      template block text
    {{/common/gmaps-custom}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
