/*jshint node:true*/
/* global require, module */
var EmberApp = require('ember-cli/lib/broccoli/ember-app');
// var map = require('broccoli-stew');


module.exports = function (defaults) {
  var app = new EmberApp(defaults, {
    'ember-cli-bootstrap-sassy': {
      'js': false
    }
  });

  // included: function included() {
  //   // this file will be loaded in FastBoot but will not be eval'd
  //   if (!process.env.EMBER_CLI_FASTBOOT) {
  //     app.import('vendor/bootstrap.js');
  //     app.import('bower_components/jquery-ui/jquery-ui.min.js');
  //     app.import('bower_components/rateYo/min/jquery.rateyo.min.js');
  //     app.import('bower_components/slick-carousel/slick/slick.js');
  //     app.import('bower_components/jquery-slimscroll/jquery.slimscroll.js');
  //   }
  // }

  app.import('bower_components/rateYo/min/jquery.rateyo.min.css');
  app.import('bower_components/jquery-ui/themes/base/jquery-ui.css');
  app.import('bower_components/jquery-ui/themes/base/theme.css');
  app.import('bower_components/slick-carousel/slick/slick-theme.scss');
  app.import('bower_components/slick-carousel/slick/slick.scss');
  app.import('bower_components/slick-carousel/slick/fonts/slick.eot');
  app.import('bower_components/slick-carousel/slick/fonts/slick.svg');
  app.import('bower_components/slick-carousel/slick/fonts/slick.ttf');
  app.import('bower_components/slick-carousel/slick/fonts/slick.woff');
  // Use `app.import` to add additional libraries to the generated
  // output files.
  //
  // If you need to use different assets in different
  // environments, specify an object as the first parameter. That
  // object's keys should be the environment name and the values
  // should be the asset to use in that environment.
  //
  // If the library that you are including contains AMD or ES6
  // modules that you would like to import into your application
  // please specify an object with the list of modules as keys
  // along with the exports of each module as its value.

  return app.toTree();
};
