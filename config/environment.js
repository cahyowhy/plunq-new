/* jshint node: true */

module.exports = function (environment) {
  var ENV = {
    modulePrefix: 'plunq-alt',
    environment: environment,
    rootURL: '/',
    locationType: 'auto',
    // fastboot: {
    //   htmlFile: 'custom-index.html'
    // },
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false
      }
    },

    APP: {
      LOGGER: true,
      apiUrl: "http://192.168.0.9:8080/plunq-api",
      cdn: "http://192.168.0.10/cdn/",
      tempAuthorization: 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxNTY2NjAwMzIzNTg0NTA1IiwiYXVkaWVuY2UiOiJtb2JpbGUiLCJjcmVhdGVkIjoxNDk5NjUxMjQ1NjYwLCJleHAiOjE0OTk3Mzc2NDV9.UJYMZRcup39VfcpDRumnz6MzemtmfcH7NAeJ-n-hBpm_dNe-9pQVsu3F5rL0BDu-CwJHBB-LUDSAAiHZYPhL8A'
      // Here you can pass flags/options to your application instance
      // when it is created
    }
  };

  ENV.googleMap = {
    apiKey: 'AIzaSyBZSIcoRHRaDCNevCUc0Bu2sONkPTfeC3s',
    libraries: ['drawing', 'visualization', 'places']
  };

  ENV.contentSecurityPolicy = {
    'default-src': "'none'",
    'script-src': "'self' 'unsafe-eval' *.googleapis.com maps.gstatic.com",
    'font-src': "'self' fonts.gstatic.com",
    'connect-src': "'self' maps.gstatic.com",
    'img-src': "'self' *.googleapis.com maps.gstatic.com csi.gstatic.com",
    'style-src': "'self' 'unsafe-inline' fonts.googleapis.com maps.gstatic.com"
  };

  if (environment === 'development') {
    ENV.APP.LOG_RESOLVER = true;
    ENV.APP.LOG_ACTIVE_GENERATION = true;
    ENV.APP.LOG_TRANSITIONS = true;
    ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
  }

  if (environment === 'production') {

  }

  return ENV;
};
