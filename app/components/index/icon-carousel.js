import Ember from 'ember';

export default Ember.Component.extend({
  init(){
    this._super(...arguments);
  },
  didInsertElement(){
    this._super(...arguments);
    Ember.$('.carousel').carousel({
      interval: 3000,
      pause: false
    })
  }
});
