import Ember from 'ember';

export default Ember.Component.extend({
  isSearchPanelDisplayed: false,
  actions: {
    toggleSearchPanel(){
      this.set("isSearchPanelDisplayed", !this.get("isSearchPanelDisplayed"))
    }
  }
});
