import Ember from 'ember';

export default Ember.Component.extend({
  init(){
    this._super(...arguments);
  },
  didInsertElement(){
    this._super(...arguments);
    const context = this;
    let container = this.$().find(".userfeedback");
    this.$().find(".modal-body").find("img").on('load', function () {
      let height = (Ember.$(this)[0].height * 1.18) - 184;
      container.slimScroll({
        height: height + "px"
      });
    });
  },
  didRender(){
    this._super(...arguments);
  }
});
