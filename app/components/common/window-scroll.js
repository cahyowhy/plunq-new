import Ember from 'ember';

export default Ember.Component.extend({
  init(){
    this._super(...arguments);
  },
  didInsertElement(){
    this._super(...arguments);
    const context = this;
    let windowPos;
    Ember.$(window).on('scroll', function () {
      windowPos = Ember.$(this).scrollTop();
      context.sendAction("action", windowPos);
    });
  }
});
