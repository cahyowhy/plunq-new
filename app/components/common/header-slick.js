import Ember from 'ember';

export default Ember.Component.extend({
  init(){
    this._super(...arguments);
  },
  didInsertElement(){
    this._super(...arguments);
    console.log($(".header-slick"));
    Ember.$(".header-slick").slick({
      infinite: true,
      fade: true,
      autoplay: true,
      autoplaySpeed: 500,
      prevArrow: Ember.$('.prev-arrow'),
      nextArrow: Ember.$('.next-arrow')
    });
  }
});
