import Ember from 'ember';

export default Ember.Component.extend({
  init(){
    this._super(...arguments);
  },
  didInsertElement(){
    this._super(...arguments);
  },
  like(){
    this.$().find("i").removeClass("fa-heart-o").addClass("fa-heart");
  },
  unlike(){
    this.$().find("i").removeClass("fa-heart").addClass("fa-heart-o");
  },
  // mouseEnter(){
  //   this.like();
  // },
  // mouseLeave(){
  //   this.unlike();
  // },
  click(){
    if (this.$().find("i").hasClass("fa-heart")) {
      this.unlike();
      this.sendAction("action", false);
    } else if (this.$().find("i").hasClass("fa-heart-o")) {
      this.like();
      this.sendAction("action", true);
    }
  }
});
