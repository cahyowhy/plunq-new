import Ember from 'ember';

export default Ember.Component.extend({
  init(){
    this._super(...arguments);
  },
  didInsertElement(){
    this._super(...arguments);
    const context = this;
    this.$().rateYo(context.get("options")).rateYo("option", "rating", context.get("rate"));
  }
});
