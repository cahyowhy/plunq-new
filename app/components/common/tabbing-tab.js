import Ember from 'ember';

export default Ember.Component.extend({
  sendValueTabHasChange(value){
    let activeTab = parseInt(value.replace("tabs-", ""));
    this.sendAction('onTabHasChange', activeTab);
  },
  didInsertElement(){
    this._super(...arguments);
    let context = this;
    Ember.$("#custom-tab").tabs({
      create: function (event, ui) {
        let activeTabID = ui.panel.attr("id");
        context.sendValueTabHasChange(activeTabID);
      },
      activate: function (event, ui) {
        let activeTabID = ui.newPanel.attr("id");
        context.sendValueTabHasChange(activeTabID);
      }
    }).addClass('ui-tabs-vertical ui-helper-clearfix');
  }
});
