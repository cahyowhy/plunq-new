import Ember from 'ember';
import ENV from '../../config/environment';

export default Ember.Component.extend({
  cdnAPI: ENV.APP.cdn,
  init(){
    this._super(...arguments);
  },
  didInsertElement(){
    this._super(...arguments);
    Ember.$(".slick-item").slick({
      dots: true,
      infinite: true,
      speed: 300,
      dots: false,
      slidesToShow: 1,
      centerMode: true,
      variableWidth: true,
      prevArrow: Ember.$('.arrow-left'),
      nextArrow: Ember.$('.arrow-right')
    });
  }
});
