import Ember from 'ember';
import gMaps from '../g-maps';

const {computed, isPresent} = Ember;

export default gMaps.extend({
  defaultGMapState: computed(function () {
    this.fitMapBound();
  }),
  fitMapBound(){
    const map = this.get('map');
    const bounds = new google.maps.LatLngBounds();
    const markers = this.get('markers').filter((marker) => {
      return isPresent(marker.lat) && isPresent(marker.lng);
    });

    const polylines = this.get('polylines').filter((polyline) => {
      return isPresent(polyline.path);
    });

    if (markers.length > 0) {
      const markerPoints = markers.map((marker) => {
        return new google.maps.LatLng(marker.lat, marker.lng);
      });
      markerPoints.forEach((point) => bounds.extend(point));
      map.fitBounds(bounds);
    }

    if (polylines !== undefined) {
      const polylinesPoints = polylines[0].path.map((polyline) => {
        return new google.maps.LatLng(polyline[0], polyline[1]);
      });
      polylinesPoints.forEach((point) => bounds.extend(point));
      map.fitBounds(bounds);
    }
  }
});
