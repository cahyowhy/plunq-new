import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

if (config.APP.LOGGER) {
  debug.enable('*');
} else {
  debug.disable();
}

Router.map(function () {
  this.route('frontend', {path: "/"});
  this.route('trip');
  this.route('tiket');
  this.route('backend.login', {path: "login"});

  this.route('backend', {path: "dashboard"}, function () {
    this.route('timeline', function () {
      this.route('local-guide');
      this.route('map-guide');
      this.route('trip-offer');
      this.route('map-guide-detail', {path: "map-guide-detail/:id"});
      this.route('profile', function () {
        this.route('album');
        this.route('map-guide-profile');
        this.route('activities');
      });
    });
  });
  this.route('page404', { path: '/*path' });
});

export default Router;
