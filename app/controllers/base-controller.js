import Ember from 'ember';

export default Ember.Mixin.create({
  init(){
    this._super(...arguments);
    const context = this;
    Ember.run.schedule("afterRender", this, function () {
      context.didInsertElement();
    });
  },
  didInsertElement(){
    this._super(...arguments); //overide controller afterrender, at once
  }
});
