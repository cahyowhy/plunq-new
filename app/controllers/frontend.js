import Ember from 'ember';

export default Ember.Controller.extend({
  actions: {
    onScroll(param){
      if (param > 550) {
        Ember.$(".navbar-wrapper").removeClass("hidden");
      } else {
        Ember.$(".navbar-wrapper").addClass("hidden");
      }
    }
  }
});
