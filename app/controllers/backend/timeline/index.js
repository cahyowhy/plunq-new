import Ember from 'ember';
import BaseController from '../../base-controller';

let sidebarTimelineWidth;
let sidebarTimelineOffsetLeft;
export default Ember.Controller.extend(BaseController, {
  biaya: '',
  totalPeserta: '',
  isMakeTripPanelDisplayed: false,
  isPlanTripPanelDisplayed: false,
  destinasi: '',
  tglMulai: '',
  tglSelesai: '',
  timelinePosts: [],
  init(){
    this._super(...arguments);
    this.addObserver('biaya', this, 'biayaHasChange')
  },
  biayaHasChange(sender, key, value, rev){
    //like ember.Observer but you can controll dom here
  },
  didInsertElement(){
    this._super(...arguments);
    const context = this;

    sidebarTimelineWidth = Ember.$(".sidebar-timeline").width() + 30;
    sidebarTimelineOffsetLeft = Ember.$(".sidebar-timeline").offset().left;
    Ember.$("#start-date").datepicker({
      minDate: 0,
      onSelect: function (dateText) {
        context.set("tglMulai", dateText);
      }
    });
    Ember.$("#end-date").datepicker({
      onSelect: function (dateText) {
        context.set("tglSelesai", dateText);
      }
    });
    Ember.$("#price-slider").slider({
      range: true,
      min: 0,
      max: 2000000,
      values: [10000, 200000],
      step: 5000,
      slide: function (event, ui) {
        Ember.$("#lo-slider-price").val(ui.values[0]);
        Ember.$("#hi-slider-price").val(ui.values[1]);
      }
    });
    Ember.$("#lo-slider-price").val(Ember.$("#price-slider").slider("values", 0));
    Ember.$("#hi-slider-price").val(Ember.$("#price-slider").slider("values", 1));

    let input = document.getElementById('destination');
    let autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.addListener('place_changed', function () {
      context.set("destinasi", autocomplete.getPlace().formatted_address);
    });
  },
  onClearField() {
    this.set('biaya', '');
    this.set('destinasi', '');
    this.set('tglMulai', '');
    this.set('tglSelesai', '');
    this.set('totalPeserta', '');
    this.set('isMakeTripPanelDisplayed', false);
    this.set('isPlanTripPanelDisplayed', false);
  },
  actions: {
    onShowMakeTrip(){
      this.set("isMakeTripPanelDisplayed", !this.get("isMakeTripPanelDisplayed"));
      this.set("isPlanTripPanelDisplayed", false);
    },
    onShowPlanTrip(){
      this.set("isMakeTripPanelDisplayed", false);
      this.set("isPlanTripPanelDisplayed", !this.get("isPlanTripPanelDisplayed"));
    },
    onSubmitPost(){
      const context = this;
      this.get("timelinePosts").pushObject({
        biaya: context.get("biaya"),
        totalPeserta: context.get("totalPeserta"),
        destinasi: context.get("destinasi"),
        tglMulai: context.get("tglMulai"),
        tglSelesai: context.get("tglSelesai"),
      });
      this.onClearField();
    },
    onScroll(param){
      if (param > 612) {
        Ember.$(".sidebar-timeline").addClass("navbar-fixed-top");
        Ember.$(".content-timeline").addClass("pull-right");
        Ember.$(".sidebar-timeline").css({
          "left": sidebarTimelineOffsetLeft + "px",
          "width": sidebarTimelineWidth + "px",
          "top": "60px"
        });
      } else {
        Ember.$(".sidebar-timeline").removeClass("navbar-fixed-top");
        Ember.$(".content-timeline").removeClass("pull-right");
        Ember.$(".sidebar-timeline").attr('style', '');
      }
    }
  }
});
