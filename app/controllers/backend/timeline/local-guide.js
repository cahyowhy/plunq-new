import Ember from 'ember';
import BaseController from '../../base-controller';

export default Ember.Controller.extend(BaseController, {
  init(){
    this._super(...arguments);
  },
  didInsertElement(){
    this._super(...arguments);
  },
  doSomething(param){
    this.debug(param);
  },
  actions: {
    doSomething(param){
      this.debug(param);
    }
  }
});
