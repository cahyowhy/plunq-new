import Ember from 'ember';

let offset = 0, limit = 9;
export default Ember.Controller.extend({
  tripServices: Ember.inject.service(),
  isLoadingDisplayed: false,
  onRequestMapTrip(){
    const context = this;
    return new Promise(function (resolve, reject) {
      context.get("tripServices").findAll(offset, limit).then(
        function (res) {
          resolve(res);
        }, function (err) {
          reject(err);
        }
      );
    });
  },
  actions: {
    onScroll(param){
      const context = this;
      let scrollContent = Ember.$(".mapguideWrapper").height() + Ember.$(".mapguideWrapper").offset().top;
      if (param + Ember.$(window).height() >= scrollContent) {
        offset += limit;
        setTimeout(function () {
          context.onRequestMapTrip().then(function (results) {
            if (results.length > 0) {
              context.set("isLoadingDisplayed", true);
            } else {
              context.set("isLoadingDisplayed", false);
            }
            results.forEach(function (item) {
              context.get("mapGuides").pushObject(item);
            });
          });
        }, 500);
      }
    }
  }
});
