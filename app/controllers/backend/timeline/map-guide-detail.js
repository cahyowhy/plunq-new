import Ember from 'ember';
import BaseController from '../../base-controller';

export default Ember.Controller.extend(BaseController, {
  gpx: "",
  lat: "",
  lng: "",
  init(){
    this._super(...arguments);
  },
  actions: {
    onFavourite(param){
      this.debug(this.get("comments"));
      this.debug(param);
    }
  }
});
