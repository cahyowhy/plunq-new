import Ember from 'ember';
import BaseController from '../base-controller';

export default Ember.Controller.extend(BaseController, {
  init(){
    this._super(...arguments);
  },
  didInsertElement(){
    this._super(...arguments);
    Ember.$('.tabs .tab').click(function () {
      if (Ember.$(this).hasClass('signin')) {
        Ember.$('.tabs .tab').removeClass('active');
        Ember.$(this).addClass('active');
        Ember.$('.cont').hide();
        Ember.$('.signin-cont').show();
      }
      if (Ember.$(this).hasClass('signup')) {
        Ember.$('.tabs .tab').removeClass('active');
        Ember.$(this).addClass('active');
        Ember.$('.cont').hide();
        Ember.$('.signup-cont').show();
      }
    });
    Ember.$('.container .bg').mousemove(function (e) {
      let amountMovedX = (e.pageX * -1 / 30);
      let amountMovedY = (e.pageY * -1 / 9);
      Ember.$(this).css('background-position', amountMovedX + 'px ' + amountMovedY + 'px');
    });
  }
});
