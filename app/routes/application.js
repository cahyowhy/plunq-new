import Ember from 'ember';
import Baserouter from './base-router';

export default Ember.Route.extend(Baserouter, {
  model(){
    return {
      isLoadingDisplayed: true,
    }
  },
  actions: {
    loading(transition){
      /*const context = this;
      transition.finally(function () {
        context.controllerFor('application').doSomething();
      });*/
    },
    willTransition(){
      /*const context = this;
      Ember.run.schedule("afterRender", this, function () {
        context.controllerFor('application').doSomething();
      });*/
    }
  },
  setupController(controller, model) {
    this.controllerFor('application').set('isLoadingDisplayed', model.isLoadingDisplayed);
  }
});
