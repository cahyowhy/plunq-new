import Ember from 'ember';

export default Ember.Route.extend({
  model(){
    return {
      trip: [
        {
          "note": "sint irure ut culpa nulla voluptate occaecat aliqua cillum nisi duis incididunt laboris enim laborum reprehenderit consectetur id ex deserunt",
          "title": "in",
          "city": "Barronett, Nevada",
          "price": 43097
        },
        {
          "note": "ullamco sint reprehenderit cillum excepteur laborum sunt occaecat eu sunt laborum nulla ipsum velit nostrud anim cupidatat nostrud commodo consectetur",
          "title": "veniam",
          "city": "Delco, Texas",
          "price": 91548
        },
        {
          "note": "Lorem culpa velit qui excepteur mollit sint anim deserunt laborum irure aliquip nulla culpa qui ad sint anim laborum duis",
          "title": "ea",
          "city": "Smock, Iowa",
          "price": 46085
        },
        {
          "note": "veniam ea eu laborum exercitation amet laborum est occaecat consequat veniam cillum ea tempor dolore ad culpa Lorem ea quis",
          "title": "cillum",
          "city": "Morgandale, Tennessee",
          "price": 45633
        },
        {
          "note": "esse est ut aliqua amet cillum elit Lorem ea veniam ex nisi ea eu laborum aliqua consequat ad ad officia",
          "title": "mollit",
          "city": "Herald, Puerto Rico",
          "price": 25869
        },
        {
          "note": "elit officia sunt veniam aliqua incididunt et incididunt culpa laborum minim do velit nisi ullamco laborum et proident commodo nostrud",
          "title": "culpa",
          "city": "Hemlock, Pennsylvania",
          "price": 61436
        },
        {
          "note": "ad culpa irure sit cupidatat magna enim qui eu officia et pariatur cupidatat elit deserunt eu reprehenderit tempor adipisicing consectetur",
          "title": "aliqua",
          "city": "Adamstown, North Carolina",
          "price": 94697
        },
        {
          "note": "non eiusmod incididunt velit sit deserunt occaecat sit ipsum eiusmod consectetur mollit id ipsum sit non cupidatat ea aliquip anim",
          "title": "enim",
          "city": "Belfair, Utah",
          "price": 57257
        },
        {
          "note": "pariatur ea et aliqua anim elit non pariatur adipisicing culpa id aliqua occaecat enim veniam officia labore proident non Lorem",
          "title": "sit",
          "city": "Orin, Missouri",
          "price": 72596
        }
      ],
      guide: [
        {
          "note": "elit ad Lorem nostrud ex esse",
          "title": "non",
          "city": "Cade, Guam",
          "price": 82490
        },
        {
          "note": "ad do pariatur reprehenderit aliqua veniam",
          "title": "sunt",
          "city": "Ogema, Maryland",
          "price": 61004
        },
        {
          "note": "veniam mollit consequat officia dolor cupidatat",
          "title": "consectetur",
          "city": "Winfred, Mississippi",
          "price": 84946
        },
        {
          "note": "elit amet reprehenderit sit officia aute",
          "title": "consectetur",
          "city": "Sunwest, Massachusetts",
          "price": 74450
        },
        {
          "note": "exercitation excepteur anim id cillum reprehenderit",
          "title": "proident",
          "city": "Austinburg, South Dakota",
          "price": 71612
        },
        {
          "note": "ipsum ex magna aliqua est quis",
          "title": "ea",
          "city": "Cresaptown, Montana",
          "price": 26255
        }
      ]
    }
  }
});
