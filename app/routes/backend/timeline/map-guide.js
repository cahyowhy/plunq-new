import Ember from 'ember';
import ENV from '../../../config/environment';

export default Ember.Route.extend( {
  tripServices: Ember.inject.service(),
  model(){
    return Ember.RSVP.hash({
      options: {
        starWidth: "13px"
      },
      mapGuides: this.get("tripServices").findAll(0, 9),
      cdnAPI: ENV.APP.cdn
    });
  },
  setupController(controller, model) {
    this.controllerFor('backend.timeline.map-guide').set('mapGuides', model.mapGuides);
    this.controllerFor('backend.timeline.map-guide').set('options', model.options);
    this.controllerFor('backend.timeline.map-guide').set('cdnAPI', model.cdnAPI);
  }
});
