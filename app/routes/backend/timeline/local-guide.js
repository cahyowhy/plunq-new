import Ember from 'ember';

export default Ember.Route.extend({
  model(){
    return {
      guide: [
        {
          "note": "nostrud do consectetur labore consectetur proident ut",
          "title": "duis",
          "city": "Convent, Nebraska",
          "price": 22794,
          "rate": 0.8833
        },
        {
          "note": "Lorem quis ullamco ex fugiat Lorem exercitation",
          "title": "enim",
          "city": "Caln, New Hampshire",
          "price": 54515,
          "rate": 3.0207
        },
        {
          "note": "enim ea quis Lorem cillum id sint",
          "title": "velit",
          "city": "Allison, Kansas",
          "price": 86727,
          "rate": 1.8016
        },
        {
          "note": "Lorem sunt non laboris voluptate id cillum",
          "title": "velit",
          "city": "Weedville, New York",
          "price": 90825,
          "rate": 0.1104
        },
        {
          "note": "incididunt ea labore ipsum deserunt exercitation veniam",
          "title": "eu",
          "city": "Moraida, West Virginia",
          "price": 10056,
          "rate": 1.6423
        },
        {
          "note": "enim ipsum id labore elit culpa culpa",
          "title": "eu",
          "city": "Biehle, Nevada",
          "price": 33423,
          "rate": 2.8669
        }
      ],
      options: {
        starWidth: "13px",
        normalFill: "#A0A0A0"
      }
    }
  },
  afterModel(model, transition){
    this.controllerFor('backend.timeline.local-guide').send('doSomething', "action controller invoked"); //invoke actions in controller after model resolved
    this.controllerFor('backend.timeline.local-guide').doSomething("method controller invoked"); //invoke actions in controller after model resolved
  }
});
