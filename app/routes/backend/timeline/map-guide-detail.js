import Ember from 'ember';
import ENV from '../../../config/environment';
import xml2js from 'npm:xml2js'
import BaseRouter from '../../base-router';

export default Ember.Route.extend(BaseRouter, {
  tripServices: Ember.inject.service(),
  beforeModel(){
    console.log("asu")
  },
  model(param){
    return Ember.RSVP.hash({
      comments: [
        {
          "name": "Callahan Boyer",
          "content": "proident quis tempor in qui magna anim"
        },
        {
          "name": "Helene Pugh",
          "content": "do do aute in cillum sint aute"
        },
        {
          "name": "Tanner Mejia",
          "content": "consectetur ex veniam excepteur qui cillum cillum"
        },
        {
          "name": "Loraine Whitfield",
          "content": "Lorem esse ea esse nostrud nulla cupidatat"
        },
        {
          "name": "Marva Mendez",
          "content": "est Lorem dolore consectetur non ut culpa"
        },
        {
          "name": "Scott Bright",
          "content": "eiusmod labore proident ullamco consequat ipsum irure"
        },
        {
          "name": "Joan Francis",
          "content": "ex ex duis voluptate culpa ad dolore"
        },
        {
          "name": "Roseann Stafford",
          "content": "duis occaecat veniam excepteur pariatur esse laboris"
        },
        {
          "name": "Fitzpatrick Emerson",
          "content": "dolore ad Lorem sit aliqua Lorem dolore"
        },
        {
          "name": "Mcdonald Owen",
          "content": "ad labore minim eiusmod laboris elit consectetur"
        },
        {
          "name": "Wanda Holcomb",
          "content": "cillum reprehenderit esse sunt excepteur voluptate adipisicing"
        },
        {
          "name": "Berger Adkins",
          "content": "veniam nisi minim laborum adipisicing minim sit"
        }
      ],
      mapDetail: this.get("tripServices").findById(param.id),
      imageThumbnail: '',
      cdnAPI: ENV.APP.cdn,
      options: {
        starWidth: "13px"
      },
      gmapAttributes: null
    });
  },
  afterModel(model, transition){
    const context = this;
    let gpx = model.mapDetail.gpx;
    let latlngtrkpt = [];
    let latlngwpt = [];
    xml2js.parseString(gpx, function (err, result) {
      result.gpx.wpt.forEach(function (item, index) {
        latlngwpt.push({id: index, lat: item['$'].lat, lng: item['$'].lon});
      });
      result.gpx.trk[0].trkseg[0].trkpt.forEach(function (item) {
        latlngtrkpt.push([item['$'].lat, item['$'].lon]);
      });
    });
    model.imageThumbnail = model.cdnAPI + model.mapDetail.venues[0].images[0].name;
    model.gmapAttributes = {
      zoom: 18,
      lat: model.mapDetail.latitude,
      lng: model.mapDetail.longitude,
      polylines: Ember.A([{id: 'unique-id', path: latlngtrkpt}]),
      markers: Ember.A(latlngwpt)
    };
    context.debug(latlngwpt);
  },
  setupController(controller, model) {
    this.controllerFor('backend.timeline.map-guide-detail').set('comments', model.comments);
    this.controllerFor('backend.timeline.map-guide-detail').set('options', model.options);
    this.controllerFor('backend.timeline.map-guide-detail').set('cdnAPI', model.cdnAPI);
    this.controllerFor('backend.timeline.map-guide-detail').set('gmapAttributes', model.gmapAttributes);
    this.controllerFor('backend.timeline.map-guide-detail').set('imageThumbnail', model.imageThumbnail);
    this.controllerFor('backend.timeline.map-guide-detail').set('mapDetail', model.mapDetail);
  }
});
