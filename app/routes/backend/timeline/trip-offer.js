import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    return {
      trip: [
        {
          "note": "velit qui incididunt et sunt occaecat aliqua labore dolore eu commodo ad nulla ea proident magna consequat dolore laborum in",
          "title": "labore",
          "city": "Boomer, Arkansas",
          "price": 48548,
          "rate": 1.1825
        },
        {
          "note": "mollit do commodo aliqua consectetur aliqua duis amet aute culpa aliqua incididunt labore exercitation ex Lorem sit laboris eiusmod occaecat",
          "title": "id",
          "city": "Leland, Pennsylvania",
          "price": 85464,
          "rate": 2.3073
        },
        {
          "note": "quis non qui pariatur amet sunt commodo deserunt voluptate pariatur enim reprehenderit Lorem dolor consequat laborum ea reprehenderit ad ipsum",
          "title": "est",
          "city": "Monument, New York",
          "price": 59539,
          "rate": 3.0786
        },
        {
          "note": "ipsum nisi id magna et pariatur exercitation veniam nisi irure velit mollit ea dolore ad deserunt nisi commodo eu ipsum",
          "title": "qui",
          "city": "Hiwasse, Delaware",
          "price": 92235,
          "rate": 2.0755
        },
        {
          "note": "irure eiusmod labore culpa id laborum eiusmod ipsum exercitation laboris aliqua eu aliquip voluptate laboris adipisicing eu minim aliquip ipsum",
          "title": "irure",
          "city": "Glendale, Georgia",
          "price": 23888,
          "rate": 0.0316
        },
        {
          "note": "incididunt adipisicing consequat sunt reprehenderit excepteur incididunt proident commodo voluptate proident laborum veniam Lorem aliquip cupidatat enim qui cillum cupidatat",
          "title": "do",
          "city": "Condon, New Mexico",
          "price": 22125,
          "rate": 3.1863
        },
        {
          "note": "consectetur proident laboris in ea sit ad nisi nostrud esse aliquip quis excepteur in reprehenderit excepteur ut incididunt dolore irure",
          "title": "commodo",
          "city": "Interlochen, Tennessee",
          "price": 24858,
          "rate": 4.2284
        },
        {
          "note": "anim occaecat est cillum exercitation veniam esse magna sunt nulla laboris reprehenderit ipsum pariatur aliqua deserunt consequat quis do elit",
          "title": "occaecat",
          "city": "Kohatk, Federated States Of Micronesia",
          "price": 51248,
          "rate": 2.8508
        },
        {
          "note": "Lorem sit nisi irure sunt mollit excepteur ea voluptate fugiat eiusmod velit aliqua adipisicing est sint tempor qui cillum nulla",
          "title": "irure",
          "city": "Levant, Alabama",
          "price": 92270,
          "rate": 3.9683
        }
      ],
      options:{
        starWidth: "13px",
        normalFill: "#A0A0A0"
      }
    }
  }
});
