import Ember from 'ember';

export default Ember.Route.extend({
  tripServices: Ember.inject.service(),
  model(){
    return Ember.RSVP.hash({
      options: {
        starWidth: "13px"
      },
      mapGuides: this.get("tripServices").findAll(0, 9)
    });
  },
  setupController(controller, model) {
    this.controllerFor('backend.timeline.profile.map-guide-profile').set('mapGuides', model.mapGuides);
    this.controllerFor('backend.timeline.profile.map-guide-profile').set('options', model.options);
  },
});
