import Ember from "ember";
import fetch from 'ember-fetch/ajax';
export default Ember.Mixin.create({
  method: {
    get: 'GET',
    post: 'POST',
    put: 'PUT',
    delete: 'DELETE'
  },
  init(api, authorization = "", param = null) { //api = url
    this.api = api;
    this.authorization = authorization;
    this.param = param;
  },
  find(param) { //param = parameter like offset or limit
    let api = this.api + param;
    return this.service(this.method.get, api, this.authorization);
  },
  convertJSON(param) {
    try {
      JSON.parse(param);
    } catch (e) {
      return JSON.stringify(param);
    }

    return param;
  },
  service(type = this.method.get, url = '', authorization = null, param = null) {
    const context = this;
    return new Ember.RSVP.Promise(
      function (resolve, reject) {
        fetch(url, {
          method: type,
          headers: {
            'Content-Type': 'application/json',
            "Authorization": authorization
          },
          body: context.convertJSON(param)
        }).then(function (response) {
          resolve(response.data);
        }).catch(function (error) {
          reject(error);
        });
      });
  },
});
