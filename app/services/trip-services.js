import Ember from 'ember';
import ENV from '../config/environment';
import BaseService from './base-services';

export default Ember.Service.extend(BaseService, {
  init(){
    this._super(ENV.APP.apiUrl + "/trips", ENV.APP.tempAuthorization);
  },
  findAll(offset, limit){
    return this.find("?share-to=ALL&offset=" + offset + "&limit=" + limit);
  },
  findById(id){
    return this.find("?id=" + id + "&share-to=ALL");
  }
});
