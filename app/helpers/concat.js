import Ember from 'ember';

export function concat(params/*, hash*/) {
  return params[0].toString() + params[1].toString();
}

export default Ember.Helper.helper(concat);
